# Starhopper Analytics

This Jenkins pipeline involve the below steps:

1) Validating the pre-flight check
2) Build and push image with Container Builder
3) Helm Charts for deployment in GKE cluster

The agent of Jenkins Pipeline used Kubernetes for creating Container template and Container builder.

The containers contain the image for gcloud, kubectl and Helm

1) Validating pre-flight check
- It will validate all the required tool for this pipeline

2) Build and push image with Container Builder
- Container Builder will build and push the images to GCR using gcloud
- imageRepo and imageTag has been defined as environment variable and the tag will increment with every build

3) Helm Chart for deployment
- Helm Chart is being used define, install and upgrade the kubernetes application

# Install Helm and Tiller on GKE cluster using cloud shell

1) Download and install the Helm binary:

wget https://storage.googleapis.com/kubernetes-helm/helm-v2.14.1-linux-amd64.tar.gz

2) Unzip the file to your local system:

tar zxfv helm-v2.14.1-linux-amd64.tar.gz
cp linux-amd64/helm .

3) Add yourself as a cluster administrator in the cluster's RBAC so that you can give Jenkins permissions in the cluster:

kubectl create clusterrolebinding cluster-admin-binding --clusterrole=cluster-admin \
        --user=$(gcloud config get-value account)
4) Grant Tiller, the server side of Helm, the cluster-admin role in your cluster:

kubectl create serviceaccount tiller --namespace kube-system
kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin \
    --serviceaccount=kube-system:tiller

5) Initialize Helm. This ensures that the Tiller is properly installed in your cluster.

./helm init --service-account=tiller
./helm repo update

6) Ensure Helm is properly installed by running the following command:

./helm version

# Restart the Jenkins on new port if required

export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/component=jenkins-master" -l "app.kubernetes.io/instance=cd" -o jsonpath="{.items[0].metadata.name}")

kubectl port-forward $POD_NAME 8080:8080 >> /dev/null &


# Helm Charts template

helmcharts/starhopper-analytics/

  Chart.yaml: A YAML file containing brief information about the starhopper-analytics chart

  README.md: A human-readable README file about chart structure

  values.yaml: The default configuration values for the starhopper-analytics chart. It provide the default values for Deployment, service and ingress yaml file.

  templates/: A directory of templates that, when combined with values, will generate valid Kubernetes manifest files.

  templates/Deployment.yml: Contain the deployment info such as imageRepo, imageTag, labels, replicaCount and so on.

  template/service.yml: It create service for the existing deployment

  template/ingress.yml: Ingress is used to expose to outside world by creating load balancer.

  template/_helpers.tpl: A place to put template helpers that you can re-use throughout the chart

# Chart.yaml

apiVersion: The chart API version, always "v1" (required)

name: The name of the chart (required)

version: A SemVer 2 version (required)

kubeVersion: A SemVer range of compatible Kubernetes versions (optional)

description: A single-sentence description of this project (optional)
keywords:
  - A list of keywords about this project (optional)

home: The URL of this project's home page (optional)

sources:
  - A list of URLs to source code for this project (optional)

maintainers: # (optional)
  - name: The maintainer's name (required for each maintainer)

    email: The maintainer's email (optional for each maintainer)

    url: A URL for the maintainer (optional for each maintainer)

icon: A URL to an SVG or PNG image to be used as an icon (optional).

appVersion: The version of the app that this contains (optional). This needn't be SemVer.

deprecated: Whether this chart is deprecated (optional, boolean)

tillerVersion: The version of Tiller that this chart requires. This should be expressed as a SemVer range: ">2.0.0" (optional)

# values.yaml

 Values passed into the template such as Deployment, service and ingress from the values.yaml file and from user-supplied files. By default, Values is empty.

# template

# Deployment.yaml

- This is used to create deployment of starhopper-analytics in GKE cluster from the image provided in spec section.
- Metadata is used for key-value pair and service is exposed to deploying using the selector which matches the key value pair of Deployment.yml
- spec provide the desired state

# service.yaml

- Way to expose starhopper-analytics running on set of pods as network service
- selector : It is used to match with the deployment label and which service to expose.
- type: NodePort - Exposes the Service on each Node’s IP at a static port

# ingress.yaml

- starhopper ingress is a kubernetes resource that encapsulates a collection of rules and configuration for routing external HTTP(S) traffic to internal services using gap load balancer.
- It will route the traffic from external world to respective services.

# Installing the Chart
To install the chart with the release name starhopper-analytics:

$ helm upgrade --install starhopper-analytics --set feApp.image.tag=${env.BUILD_NUMBER} ./helmcharts/starhopper-analytics

The command deploys starhopper-analytics on the Kubernetes cluster in the default configuration. 

The values.yaml parameter can be overridden using --set value.

Tip: List all releases using helm list

# Uninstalling the Chart
To uninstall/delete the starhopper-analytics deployment:

$ helm delete starhopper-analytics --purge
The command removes all the Kubernetes components associated with the chart and deletes the release.  



