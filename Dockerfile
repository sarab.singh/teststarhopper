FROM gcr.io/google-appengine/jetty
ARG ritual_environment=sandbox
ADD target/Starhopper-analytics-legacy.war $JETTY_BASE/webapps/root.war
WORKDIR $JETTY_BASE

ENV ritual_environment=${ritual_environment}
ENV ritual_platform=gke

RUN java -jar $JETTY_HOME/start.jar --approve-all-licenses --add-to-startd=jmx,stats,hawtio && chown -R jetty:jetty $JETTY_BASE
